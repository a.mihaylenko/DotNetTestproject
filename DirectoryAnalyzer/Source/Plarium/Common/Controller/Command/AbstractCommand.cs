﻿using Plarium.Common.Controller.Command.Attributes;
using Plarium.Common.Model;
using Plarium.Common.View;
using System;
using System.Threading;
using static Plarium.Common.Enums;

namespace Plarium.Common.Controller.Command
{
    /// <summary>
    /// Abstract command implementation, implement ICommand interface, 
    /// please inherit this class for create concrete app command.
    /// </summary>
    public abstract class AbstractCommand : ICommand
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public AbstractCommand()
        {
            ParseAttributes();
            if (!_isExecuteInMainThread)
                AutoResetEvent = new AutoResetEvent(false);
        }

        #endregion
        #region Abstract properties and methods
        /// <summary>
        /// Abstract execute method, execute async, 
        /// please Invoke InheritExecuteMethodComplete event when is complete for async implementation,
        /// it's important, becouse command can executes not in a main thread.
        /// </summary>
        protected abstract void ExecuteInheritMethod();

        #endregion
        #region Private & protected properties
        private Thread _commandThread;
        private CommandState _commandState = CommandState.CommandInited;
        private bool _isExecuteInMainThread;

        public event Action<ICommand, CommandState> CommandStateChanged;
        #endregion
        #region Public methods & properties

        public int Id { private set; get; }
        public CommandState CommandState {
            get
            {
                return _commandState;
            }
            protected set
            {
                if (_commandState != value)
                {
                    _commandState = value;
                    if (!_isExecuteInMainThread)
                    {
                        switch (_commandState)
                        {
                            case CommandState.CommandStarted:
                                _commandThread.Start();
                                break;
                            case CommandState.CommandAborted:
                            case CommandState.CommandCompleted:
                                break;
                        }
                    }
                    CommandStateChanged?.Invoke(this, _commandState);
                }
            }
        }

        /// <summary>
        /// Auto reset event for syncronize async commands
        /// </summary>
        public AutoResetEvent AutoResetEvent { get; private set; }

        /// <summary>
        /// Flag, for save command in a memory after execution,
        /// placed on implementation class attributes.
        /// </summary>
        public bool IsSaveInMemory { private set;  get; }

        /// <summary>
        /// Dispose all allocated data.
        /// </summary>
        public void Dispose()
        {
            if (AutoResetEvent!=null)
            {
                AutoResetEvent.Close();
                AutoResetEvent.Dispose();
                AutoResetEvent = null;
            }
            Abort();
            _commandThread = null;
        }

        public void Execute()
        {
            if (_commandThread!=null)
            {
            }
            else
            {
                if (!_isExecuteInMainThread)
                {
                    _commandThread = new Thread(ExecuteInheritMethod);
                    _commandThread.IsBackground = true;
                    CommandState = CommandState.CommandStarted;

                }
                else
                {
                    CommandState = CommandState.CommandStarted;

                    ExecuteInheritMethod();
                }

            }
        }
        /// <summary>
        /// Abort current command if they in execution state.
        /// </summary>
        public void Abort()
        {
            CommandState = CommandState.CommandAborted;
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Parse inherit attributes.
        /// </summary>
        private void ParseAttributes()
        {
            var attributes = GetType().GetCustomAttributes(typeof(CommandAttribute), true);
            if (attributes.Length > 0 && attributes[0] is CommandAttribute)
            {
                CommandAttribute atr = attributes[0] as CommandAttribute;
                _isExecuteInMainThread = atr.IsExecuteInMainThread;
                Id = atr.Id;
                IsSaveInMemory = atr.IsSaveInMemory;
            }
        }
        #endregion
    }
}
