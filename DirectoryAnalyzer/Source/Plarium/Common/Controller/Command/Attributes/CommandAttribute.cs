﻿using System;

namespace Plarium.Common.Controller.Command.Attributes
{
    /// <summary>
    /// Sealed command attribute data class.
    /// </summary>
    public sealed class CommandAttribute : Attribute
    {
        public bool IsSaveInMemory { set; get; }
        public bool IsExecuteInMainThread { set; get; }
        public int Id { set; get; }
    }
}
