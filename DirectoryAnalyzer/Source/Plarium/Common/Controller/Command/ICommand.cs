﻿using Plarium.Common.Model;
using Plarium.Common.View;
using System;
using System.Threading;
using static Plarium.Common.Enums;

namespace Plarium.Common.Controller.Command
{
    /// <summary>
    /// Common interface of microcommand pattern.
    /// </summary>
    public interface ICommand:IDisposable
    {
        event Action<ICommand, CommandState> CommandStateChanged;

        /// <summary>
        /// Auto reset event for syncronize async commands
        /// </summary>
        AutoResetEvent AutoResetEvent { get; }

        /// <summary>
        /// Current command state
        /// </summary>
        CommandState CommandState { get; }

        /// <summary>
        /// Current command Is, placed on implementation class attributes.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Flag, for save command in a memory after execution,
        /// placed on implementation class attributes.
        /// </summary>
        bool IsSaveInMemory { get; }

        /// <summary>
        /// Execute current command.
        /// </summary>
        void Execute();
        

        /// <summary>
        /// Abort current command if they in execution state.
        /// </summary>
        void Abort();
    }
}
