﻿using Plarium.Common.Controller.Command;
using Plarium.Common.Extensions;
using Plarium.Common.Model;
using Plarium.Common.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static Plarium.Common.Enums;

namespace Plarium.Common.Controller
{
    /// <summary>
    /// Base implementation of IController interface 
    /// </summary>
    public class Controller : IController
    {
        #region Private properties
        private List<Type> _registeredCommands;
        private List<ICommand> _executionOrInMemoryCommands;
        private IModel _model;
        private IView _view;
        #endregion

        #region Constructor
        public Controller(IModel model, IView view)
        {
            _model = model;
            _view = view;

            _executionOrInMemoryCommands = new List<ICommand>();
            _registeredCommands = new List<Type>();

            _view.SetController(this);
        }
        #endregion

        #region IController interface implementation methods

        /// <summary>
        /// Event for check when command state was changed.
        /// </summary>
        public event Action<int, CommandState> CommandStateChanged;

        /// <summary>
        /// Checks for the registered command earlier.
        /// </summary>
        /// <param name="command"> Command type, Commands.ICommand implementation </param>
        public bool HasRegisteredCommand(Type command)
        {
            return _registeredCommands.Exists(x => x.Equals(command));
        }

        /// <summary>
        /// Register command
        /// </summary>
        /// <param name="command"> Command type, Commands.ICommand implementation </param>
        public void RegisterCommand(Type command)
        {
            if (!command.IsClass || !typeof(ICommand).IsAssignableFrom(command))
            {
                throw new Exception("IController->RegisterCommand, can't register command of:" + command.FullName + ", it's not a Class implementation of ICommand interface, please check it");
            }

            if (!HasRegisteredCommand(command))
                _registeredCommands.Add(command);
        }

        /// <summary>
        /// Remove command
        /// </summary>
        /// <param name="command"> Registered command type </param>
        public void RemoveCommand(Type command)
        {
            if (HasRegisteredCommand(command))
                _registeredCommands.Remove(command);
        }

        /// <summary>
        /// Execute existen command
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if command was successful find and executed</returns>
        public bool ExecuteCommand(int id)
        {
            ICommand command = _executionOrInMemoryCommands.FirstOrDefault(x => x.Id == id);
            if (command == null)
            {
                Type type = _registeredCommands.FirstOrDefault(x => x.GetIdFromAttributes() == id);
                if (type != default(Type))
                    command = Activator.CreateInstance(type, this, _model, _view) as ICommand;
                _executionOrInMemoryCommands.Add(command);
            }

            if (command == null)
            {
                Console.WriteLine(this.GetType().FullName + "->ExecuteCommand() error, command with id:" + id + " not registered");
                return false;
            }

            command.CommandStateChanged += Command_CommandStateChanged;
            command.Execute();

            return true;

        }

        /// <summary>
        /// Try to remove running command.
        /// </summary>
        /// <param name="id"></param>
        public void PermanentlyRemoveRunningCommand(int id)
        {
            ICommand command = TakeRunningCommand(id);
            if (command != null)
            {
                command.Dispose();
                _executionOrInMemoryCommands.Remove(command);
            }
        }

        /// <summary>
        /// Check command by id from runing state.
        /// </summary>
        /// <param name="id"> Command id </param>
        /// <returns> true if command is ran. </returns>
        public bool IsCommandRunning(int id)
        {
             return TakeRunningCommand(id)!= null;
        }
        /// <summary>
        /// Search running command by type.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ICommand implementation instance </returns>
        /// <default>null</default>
        public ICommand TakeRunningCommand(int id)
        {
            return _executionOrInMemoryCommands.FirstOrDefault(x => x.Id == id);
        }
        #endregion
        #region Private methods
        private void Command_CommandStateChanged(ICommand command, CommandState state)
        {

            CommandStateChanged?.Invoke(command.Id, command.CommandState);

            if (command.CommandState == CommandState.CommandCompleted)
            {
                command.CommandStateChanged -= Command_CommandStateChanged;
                if (!command.IsSaveInMemory)
                {
                    command.Dispose();
                    _executionOrInMemoryCommands.Remove(command);
                }
            }
        }
        #endregion
    }
}
