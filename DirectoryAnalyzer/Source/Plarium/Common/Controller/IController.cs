﻿using Plarium.Common.Controller.Command;
using System;
using static Plarium.Common.Enums;

namespace Plarium.Common.Controller
{
    /// <summary>
    /// Base interface for common controller
    /// </summary>
    public interface IController
    {
        /// <summary>
        /// Event for check when command state was changed.
        /// </summary>
        event Action<int, CommandState> CommandStateChanged;

        /// <summary>
        /// Checks for the registered command earlier.
        /// </summary>
        /// <param name="command"> Command type, Commands.ICommand implementation </param>
        bool HasRegisteredCommand(Type command);

        /// <summary>
        /// Register command
        /// </summary>
        /// <param name="command"> Command type, Commands.ICommand implementation </param>
        void RegisterCommand( Type command );

        /// <summary>
        /// Remove command
        /// </summary>
        /// <param name="command"> Registered command type </param>
        void RemoveCommand(Type command);

        /// <summary>
        /// Execute existen command
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if command was successful find and executed</returns>
        bool ExecuteCommand(int id);

        /// <summary>
        /// Check command by id from runing state.
        /// </summary>
        /// <param name="id"> Command id </param>
        /// <returns> true if command is ran. </returns>
        bool IsCommandRunning( int id );

        /// <summary>
        /// Try to remove running command
        /// </summary>
        /// <param name="id"></param>
        void PermanentlyRemoveRunningCommand(int id);

        /// <summary>
        /// Search running command by type.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>ICommand implementation instance </returns>
        /// <default>null</default>
        ICommand TakeRunningCommand(int id);
    }
}
