﻿using Plarium.Common.Controller.Command.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plarium.Common.Extensions
{
    public static class CommandTypeExtension
    {
        public static int GetIdFromAttributes(this Type type, int defaultId=-1)
        {
            var customAttributes = type.GetCustomAttributes(typeof(CommandAttribute), true);
            if (customAttributes.Length > 0)
                return (customAttributes[0] as CommandAttribute).Id;
            else
                return defaultId;
        }
    }
}
