﻿using Plarium.Common.Controller;
using System;

namespace Plarium.Common.View
{
    public interface IView:IDisposable
    {
        void SetController( IController controller );
    }
}
