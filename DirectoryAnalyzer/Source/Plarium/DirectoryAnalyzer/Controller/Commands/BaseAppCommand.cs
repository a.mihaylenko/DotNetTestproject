﻿using Plarium.Common.Controller;
using Plarium.Common.Controller.Command;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.View;
using System;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    public class BaseAppCommand : AbstractCommand
    {
        #region Constructor
        public BaseAppCommand(IController controller, AppModel model, DirectoryAnalyzerView view):base()
        {
            Controller = controller;
            Model = model;
            View = view;
        }
        #endregion
        /// <summary>
        /// The application model.
        /// </summary>
        protected AppModel Model { get; private set; }

        /// <summary>
        /// The application view.
        /// </summary>
        protected DirectoryAnalyzerView View { get; private set; }
        /// <summary>
        /// The application controller.
        /// </summary>
        protected IController Controller { get; private set; }

        /// <inheritdoc />
        protected override void ExecuteInheritMethod(){}

    }
}
