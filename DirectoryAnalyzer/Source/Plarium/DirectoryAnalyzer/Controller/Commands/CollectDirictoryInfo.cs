﻿using Plarium.Common.Controller;
using Plarium.Common.Controller.Command.Attributes;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.Model.Vo;
using Plarium.DirectoryAnalyzer.View;
using System;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using static Plarium.DirectoryAnalyzer.Enums;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    [Command(Id = (int)Command.CollectDirectoryInfo, IsExecuteInMainThread = false, IsSaveInMemory = false)]
    public sealed class CollectDirectoryInfo : BaseAppCommand
    {
        private delegate void TreeViewPanel_UpdateInfo(string path);
        private TreeViewPanel_UpdateInfo _treeViewPanel_UpdateInfo_Delegate;
        #region Construcltor
        public CollectDirectoryInfo(IController controller, AppModel model, DirectoryAnalyzerView view) : base(controller, model, view)
        {
        }
        #endregion

        protected override void ExecuteInheritMethod()
        {
            _treeViewPanel_UpdateInfo_Delegate = new TreeViewPanel_UpdateInfo(TreeViewPanelUpdateInfo);
            CollectFileInfoVO(Model.DirectoryPath);
            Model.IsAnalyseFinished = true;
            View.BeginInvoke(_treeViewPanel_UpdateInfo_Delegate, "Done.");
        }

        private void CollectFileInfoVO( string path, string parentId = null )
        {

            FileInfoNodeVO fiNVO = new FileInfoNodeVO(parentId, Guid.NewGuid().ToString());
            fiNVO.ParseFromPath(path);
            Model.FileInfoVO = fiNVO;
            AutoResetEvent.Set();
            AutoResetEvent.WaitOne();
            View.BeginInvoke(_treeViewPanel_UpdateInfo_Delegate, path);


            if (fiNVO.AccessRights.Read && (fiNVO.FileAttributes & FileAttributes.Directory) == FileAttributes.Directory)
            {
                string[] children = Directory.GetDirectories(path).Concat(Directory.GetFiles(path)).ToArray();
                foreach (string childPath in children)
                {
                    CollectFileInfoVO(childPath, fiNVO.Id);
                }
            }
        }
        private void TreeViewPanelUpdateInfo(string info)
        {
            View.TreeViewPanel_UpdateInfo(info);

            if (Model.IsAnalyseFinished)
            {
                AutoResetEvent?.Set();
                _treeViewPanel_UpdateInfo_Delegate = null;
                View.SetEnableBackToIntro(true);
                CommandState = Common.Enums.CommandState.CommandCompleted;
            }
        }
    }
}
