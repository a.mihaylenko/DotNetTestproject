﻿using Plarium.Common.Controller;
using Plarium.Common.Controller.Command.Attributes;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.Model.Vo;
using Plarium.DirectoryAnalyzer.View;
using System;
using System.Threading;
using static Plarium.DirectoryAnalyzer.Enums;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    [Command(Id = (int)Command.DrawCollectedFileInfoCommand, IsExecuteInMainThread = false, IsSaveInMemory = false)]
    public sealed class DrawCollectedFileInfoCommand : BaseAppCommand
    {
        private AutoResetEvent _waitForSaveCollectedDirectoryInfo;
        private AutoResetEvent _waitForCollectDirectoryInfo;
        private DrawDelegate _drawDelegate;
        private delegate void DrawDelegate(FileInfoNodeVO fileInfoNodeVO);
        #region Construcltor
        public DrawCollectedFileInfoCommand(IController controller, AppModel model, DirectoryAnalyzerView view) : base(controller, model, view)
        {
            _waitForCollectDirectoryInfo = controller.TakeRunningCommand((int)Command.CollectDirectoryInfo).AutoResetEvent;
            _waitForSaveCollectedDirectoryInfo = controller.TakeRunningCommand((int)Command.SaveCollectedFileInfoToXML).AutoResetEvent;
        }
        #endregion

        protected override void ExecuteInheritMethod()
        {
            _drawDelegate = new DrawDelegate(Draw);

            while (!Model.IsAnalyseFinished)
            {
                _waitForSaveCollectedDirectoryInfo.WaitOne();
                View.BeginInvoke(_drawDelegate, Model.FileInfoVO);
            }
            _waitForSaveCollectedDirectoryInfo.WaitOne();
            _drawDelegate = null;
            CommandState = Common.Enums.CommandState.CommandCompleted;
        }
        private void Draw( FileInfoNodeVO fileInfoNodeVO )
        {
            if (fileInfoNodeVO == null)
                return;
            View.DrawViewTreeNode(fileInfoNodeVO);
            _waitForCollectDirectoryInfo.Set();
        }
    }
}
