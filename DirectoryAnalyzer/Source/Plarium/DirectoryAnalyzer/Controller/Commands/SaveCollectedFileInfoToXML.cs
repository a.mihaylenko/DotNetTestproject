﻿
using Plarium.Common.Controller;
using Plarium.Common.Controller.Command.Attributes;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.View;
using System.Xml;
using System.Threading;
using static Plarium.DirectoryAnalyzer.Enums;
using System.Xml.Serialization;
using System.IO;
using System;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    [Command(Id = (int)Command.SaveCollectedFileInfoToXML, IsExecuteInMainThread = false, IsSaveInMemory = false)]
    public sealed class SaveCollectedFileInfoToXML : BaseAppCommand
    {
        private AutoResetEvent _waitForCollectDirectoryInfo;
        public SaveCollectedFileInfoToXML(IController controller, AppModel model, DirectoryAnalyzerView view) : base(controller, model, view)
        {
            _waitForCollectDirectoryInfo = controller.TakeRunningCommand((int)Command.CollectDirectoryInfo).AutoResetEvent;
        }

        protected override void ExecuteInheritMethod()
        {
            Model.CreateNewXMLDocument();
            while (!Model.IsAnalyseFinished)
            {
                _waitForCollectDirectoryInfo.WaitOne();//Wait for collect new file info value object
                Model.SaveCurrentFileInfoToXMLDocument();
                AutoResetEvent.Set();//Run to draw collected directory info
            }
            _waitForCollectDirectoryInfo.WaitOne();
            AutoResetEvent.Set();
            CommandState = Common.Enums.CommandState.CommandCompleted;
        }

    }
}
