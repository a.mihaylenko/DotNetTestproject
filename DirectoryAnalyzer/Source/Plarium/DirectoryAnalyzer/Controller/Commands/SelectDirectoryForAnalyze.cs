﻿using Plarium.Common.Controller;
using Plarium.Common.Controller.Command.Attributes;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.View;
using System;
using System.Windows.Forms;
using static Plarium.DirectoryAnalyzer.Enums;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    [Command(Id = (int)Command.SelectDirectoryForAnalyze, IsExecuteInMainThread = true, IsSaveInMemory = false)]
    public sealed class SelectDirectoryForAnalyze : BaseAppCommand
    {
        #region Private properties
        FolderBrowserDialog _folderBrowser;
        #endregion

        #region Constructor
        public SelectDirectoryForAnalyze(IController controller, AppModel model, DirectoryAnalyzerView view) : base(controller, model, view)
        {
        }
        #endregion
        #region Command execution
        protected override void ExecuteInheritMethod()
        {
            if (_folderBrowser == null)
            {
                _folderBrowser = new FolderBrowserDialog();
                _folderBrowser.RootFolder = Environment.SpecialFolder.MyComputer;
                _folderBrowser.ShowNewFolderButton = false;
            }

            DialogResult result = _folderBrowser.ShowDialog();
            if (result == DialogResult.OK)
            {
                Model.DirectoryPath = _folderBrowser.SelectedPath;
                View.IntoPanel_UpdateDirPathInfo(Model.DirectoryPath);
            }
            else
            {
                Model.DirectoryPath = null;
                View.IntoPanel_UpdateDirPathInfo(null);
            }
            View.IntoPanel_SetVisibleBtnOpenNextPanel(Model.IsAllPathsValid);
            CommandState = Common.Enums.CommandState.CommandCompleted;
        }
        #endregion
    }
}
