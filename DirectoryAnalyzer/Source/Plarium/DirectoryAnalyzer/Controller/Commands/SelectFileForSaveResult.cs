﻿using Plarium.Common.Controller;
using Plarium.Common.Controller.Command.Attributes;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.View;
using System.IO;
using System.Windows.Forms;
using static Plarium.DirectoryAnalyzer.Enums;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    [Command(Id = (int)Command.SelectFileForSaveResult, IsExecuteInMainThread = true, IsSaveInMemory = false)]
    public sealed class SelectFileForSaveResult : BaseAppCommand
    {
        #region Constructor
        public SelectFileForSaveResult(IController controller, AppModel model, DirectoryAnalyzerView view) : base(controller, model, view)
        {
        }
        #endregion
        #region Command execution
        private SaveFileDialog _fileDialog;
        /// <inheritdoc />
        protected override void ExecuteInheritMethod()
        {
            if (_fileDialog == null)
            {
                _fileDialog = new SaveFileDialog();
                _fileDialog.CreatePrompt    =
                _fileDialog.OverwritePrompt =
                _fileDialog.AddExtension    = true;
                _fileDialog.Filter = "Xml Files (*.xml)|*.xml";
            }
            DialogResult result = _fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                Model.ResultFilePath = _fileDialog.FileName;
                View.IntoPanel_UpdateFilePathInfo(Model.ResultFilePath);
            }
            else
            {
                Model.ResultFilePath = null;
                View.IntoPanel_UpdateFilePathInfo(null);
            }
            View.IntoPanel_SetVisibleBtnOpenNextPanel(Model.IsAllPathsValid);
            CommandState = Common.Enums.CommandState.CommandCompleted;
        }

        #endregion
    }
}
