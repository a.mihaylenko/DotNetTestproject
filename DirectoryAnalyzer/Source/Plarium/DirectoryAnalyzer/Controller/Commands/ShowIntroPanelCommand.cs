﻿using Plarium.Common.Controller;
using Plarium.Common.Controller.Command.Attributes;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.View;
using static Plarium.DirectoryAnalyzer.Enums;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    [Command(Id = (int)Command.ShowViewIntroPanel, IsExecuteInMainThread =true, IsSaveInMemory =false)]
    public sealed class ShowIntroPanelCommand : BaseAppCommand
    {
        private delegate void ShowIntroPanelDelegate();
        #region Construcltor
        public ShowIntroPanelCommand(IController controller, AppModel model, DirectoryAnalyzerView view) : base(controller, model, view)
        {
        }
        #endregion

        protected override void ExecuteInheritMethod()
        {

            Controller.PermanentlyRemoveRunningCommand((int)Command.CollectDirectoryInfo);
            Controller.PermanentlyRemoveRunningCommand((int)Command.SaveCollectedFileInfoToXML);
            Controller.PermanentlyRemoveRunningCommand((int)Command.DrawCollectedFileInfoCommand);
            Model.IsAnalyseFinished = true;
            Model.ClearXMLDocument();
            Model.FileInfoVO = null;
            View.ChangeViewPanel(AppViewPanel.IntroPanel);
            View.IntoPanel_SetVisibleBtnOpenNextPanel(Model.IsAllPathsValid);

            CommandState = Common.Enums.CommandState.CommandCompleted;
        }
    }
}
