﻿using Plarium.Common.Controller;
using Plarium.Common.Controller.Command.Attributes;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.View;
using static Plarium.DirectoryAnalyzer.Enums;

namespace Plarium.DirectoryAnalyzer.Controller.Commands
{
    [Command(Id = (int)Command.ShowViewResultTreePanel, IsExecuteInMainThread = true, IsSaveInMemory = false)]
    public class ShowViewResultTreePanelCommand : BaseAppCommand
    {
        #region Constructor
        public ShowViewResultTreePanelCommand(IController controller, AppModel model, DirectoryAnalyzerView view) : base(controller, model, view)
        {
        }
        #endregion

        protected override void ExecuteInheritMethod()
        {
            View.ChangeViewPanel(AppViewPanel.TreeViewPanel);
            View.SetEnableBackToIntro(false);
            Model.IsAnalyseFinished = false;

            Controller.ExecuteCommand((int)Command.CollectDirectoryInfo);
            Controller.ExecuteCommand((int)Command.SaveCollectedFileInfoToXML);
            Controller.ExecuteCommand((int)Command.DrawCollectedFileInfoCommand);

            CommandState = Common.Enums.CommandState.CommandCompleted;
        }
    }
}