﻿namespace Plarium.DirectoryAnalyzer
{
    public class Enums
    {
        public enum Command
        {
            ShowViewIntroPanel,
            ShowViewResultTreePanel,
            SelectDirectoryForAnalyze,
            SelectFileForSaveResult,
            CollectDirectoryInfo,
            SaveCollectedFileInfoToXML,
            DrawCollectedFileInfoCommand
        }
        public enum AppViewPanel
        {
            IntroPanel,
            TreeViewPanel
        }
    }
}
