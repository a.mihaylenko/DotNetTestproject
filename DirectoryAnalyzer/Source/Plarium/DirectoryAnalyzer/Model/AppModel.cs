﻿using Plarium.Common.Model;
using Plarium.DirectoryAnalyzer.Model.Extensions;
using Plarium.DirectoryAnalyzer.Model.Vo;
using System;
using System.IO;
using System.Threading;
using System.Xml;

namespace Plarium.DirectoryAnalyzer.Model
{
    public class AppModel : IModel
    {
        private XmlDocument _xmlDocumentResult;

        /// <summary>
        /// Directory path, for do recursive analyze files and folders
        /// </summary>
        public string DirectoryPath { set; get; }

        /// <summary>
        /// File path, for save result.
        /// </summary>
        public string ResultFilePath { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsAllPathsValid
        {
            get
            {
                return !string.IsNullOrEmpty(DirectoryPath) && !string.IsNullOrEmpty(ResultFilePath);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsAnalyseFinished { get; set; }

        public FileInfoNodeVO FileInfoVO { get; set; }

        public void SaveCurrentFileInfoToXMLDocument()
        {
            if (IsAnalyseFinished)
                return;
            if (FileInfoVO != null)
            {
                XmlNode parent = null;
                if (FileInfoVO.ParenId == null)
                {
                    parent = _xmlDocumentResult;
                }
                else
                {
                    foreach (XmlNode xCheck in _xmlDocumentResult.GetElementsByTagName("File"))
                    {
                        if (xCheck.Attributes["Id"].Value == FileInfoVO.ParenId)
                        {
                            parent = xCheck;
                            break;
                        }
                    }
                }

                if (parent == null)
                    parent = _xmlDocumentResult;

                XmlNode newElement = FileInfoVO.Serialize();
                newElement = _xmlDocumentResult.ImportNode(newElement, true);
                parent.AppendChild(newElement);
                _xmlDocumentResult.Save(ResultFilePath);
            }
        }

        public void CreateNewXMLDocument()
        {
            ClearXMLDocument();
            _xmlDocumentResult = new XmlDocument();
        }
        public void ClearXMLDocument()
        {

            _xmlDocumentResult?.RemoveAll();
            _xmlDocumentResult = null;
            FileInfoVO = null;
        }
        /// <inheritdoc />
        public void Dispose()
        {
        }
    }
}
