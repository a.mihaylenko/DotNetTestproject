﻿using Plarium.DirectoryAnalyzer.Model.Vo;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Plarium.DirectoryAnalyzer.Model.Extensions
{
    public static class ValueObjectToXMLExtension
    {
        public static XmlElement Serialize(this FileInfoNodeVO fileInfoNodeVO)
        {
            XmlElement resultNode = null;
            XmlSerializer xmlSerializer = new XmlSerializer(fileInfoNodeVO.GetType());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                try
                {
                    xmlSerializer.Serialize(memoryStream, fileInfoNodeVO);
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
                memoryStream.Position = 0;
                XmlDocument doc = new XmlDocument();
                doc.Load(memoryStream);
                resultNode = doc.DocumentElement;
            }
            return resultNode;
        }
    }
}
