﻿using Plarium.DirectoryAnalyzer.Utility;
using System;
using System.IO;
using System.Security.AccessControl;
using System.Xml;
using System.Xml.Serialization;

namespace Plarium.DirectoryAnalyzer.Model.Vo
{
    [Serializable]
    [XmlRoot(ElementName = "File")]
    public class FileInfoNodeVO
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileInfoNodeVO() { }
        /// <summary>
        /// Constructor for directory
        /// </summary>
        /// <param name="parentId"> unique parent id </param>
        /// <param name="path"> current path to file or directory </param>
        public FileInfoNodeVO(string parentId, string id)
        {
            ParenId = parentId;
            Id = id;
        }

        public void ParseFromPath(string path)
        {
            AccessRights = new AccessFileRights(path);
            if (AccessRights.Read)
            {
                FileAttributes = File.GetAttributes(path);
                Owner = FileAccessUtility.GetOwner(path);
                Attributes = new Attributes(FileAttributes);

                if ((FileAttributes & FileAttributes.Directory) == FileAttributes.Directory)
                    ParseDirectory(path);
                else
                    ParseFile(path);
            }
            else
            {
                Name = Path.GetFileName(path);
            }
        }

        #endregion
        #region Private methods
        private void ParseFile( string path )
        {
            FileInfo fi = new FileInfo(path);
            Length = fi.Length;
            Parse(fi);
        }
        private void ParseDirectory( string path )
        {
            DirectoryInfo di = new DirectoryInfo(path);
            Parse(di);
        }
        private void Parse(FileSystemInfo fileSystemInfo)
        {
            Name = fileSystemInfo.Name;
            CreationDate = fileSystemInfo.CreationTime;
            ModificationDate = fileSystemInfo.LastWriteTime;
            LastAccessDate = fileSystemInfo.LastAccessTime;
        }


        #endregion
        #region Public properties
        [XmlIgnore]
        public string ParenId { get; set; }
        [XmlAttribute]
        public string Id { get; set; }

        /// <summary>
        /// Current file or directory name.
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }

        /// <summary>
        /// Date of creation
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Date of modifications.
        /// </summary>
        public DateTime ModificationDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastAccessDate { get; set; }

        [XmlIgnore]
        public FileAttributes FileAttributes { get; set; }

        public Attributes Attributes { get; set; }

        public string Owner { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long Length { get; set; }

        public AccessFileRights AccessRights;

        #endregion
    }
    [Serializable]
    public class AccessFileRights
    {
        public AccessFileRights() { }

        public AccessFileRights(string path)
        {
            Read = FileAccessUtility.IsReadable(path);
            Write = FileAccessUtility.IsWriteable(path);
            Delete = FileAccessUtility.IsDeletable(path);
        }

        public bool Read { set; get; }
        public bool Write { set; get; }
        public bool Delete { set; get; }

    }
    [Serializable]
    public class Attributes
    {
        public Attributes() { }

        public Attributes(FileAttributes attributes)
        {
            ReadOnly = (attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly;
            Hidden = (attributes & FileAttributes.Hidden) == FileAttributes.Hidden;
            System = (attributes & FileAttributes.System) == FileAttributes.System;
        }
        public bool ReadOnly { set; get; }
        public bool Hidden { set; get; }
        public bool System { set; get; }
    }



}
