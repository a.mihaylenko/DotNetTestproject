﻿using Plarium.Common.Controller;
using Plarium.Common.View;
using Plarium.Common.Model;
using System;
using System.Windows.Forms;
using Plarium.DirectoryAnalyzer.View;
using Plarium.DirectoryAnalyzer.Model;
using Plarium.DirectoryAnalyzer.Controller.Commands;

namespace DirectoryAnalyzer
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DirectoryAnalyzerView view = new DirectoryAnalyzerView();
            IModel model = new AppModel();
            IController controller = new Controller(model, view);

            RegisterAppCommands(controller);

            Application.Run(view);
        }

        /// <summary>
        /// Register all application commands.
        /// </summary>
        /// <param name="appController"></param>
        static void RegisterAppCommands( IController appController )
        {
            appController.RegisterCommand(typeof(ShowIntroPanelCommand));
            appController.RegisterCommand(typeof(ShowViewResultTreePanelCommand));
            appController.RegisterCommand(typeof(SaveCollectedFileInfoToXML));
            appController.RegisterCommand(typeof(CollectDirectoryInfo));
            appController.RegisterCommand(typeof(SelectDirectoryForAnalyze));
            appController.RegisterCommand(typeof(SelectFileForSaveResult));
            appController.RegisterCommand(typeof(DrawCollectedFileInfoCommand));
        }

    }
}
