﻿namespace Plarium.DirectoryAnalyzer.View
{
    partial class DirectoryAnalyzerView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DirectoryAnalyzerView));
            this.IntroPanel = new System.Windows.Forms.Panel();
            this.btnOpenDirectorySearchPanel = new System.Windows.Forms.Button();
            this.labelFilePathInfo = new System.Windows.Forms.Label();
            this.btnSelectFileForSave = new System.Windows.Forms.Button();
            this.labelDirPathInfo = new System.Windows.Forms.Label();
            this.btnSelectDir = new System.Windows.Forms.Button();
            this.TreeViewPanel = new System.Windows.Forms.Panel();
            this.labelTreeViewInformation = new System.Windows.Forms.Label();
            this.treeViewResult = new System.Windows.Forms.TreeView();
            this.btnBackToIntroPanel = new System.Windows.Forms.Button();
            this.IntroPanel.SuspendLayout();
            this.TreeViewPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // IntroPanel
            // 
            this.IntroPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.IntroPanel.Controls.Add(this.btnOpenDirectorySearchPanel);
            this.IntroPanel.Controls.Add(this.labelFilePathInfo);
            this.IntroPanel.Controls.Add(this.btnSelectFileForSave);
            this.IntroPanel.Controls.Add(this.labelDirPathInfo);
            this.IntroPanel.Controls.Add(this.btnSelectDir);
            this.IntroPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.IntroPanel.Location = new System.Drawing.Point(0, 0);
            this.IntroPanel.Name = "IntroPanel";
            this.IntroPanel.Size = new System.Drawing.Size(457, 372);
            this.IntroPanel.TabIndex = 0;
            // 
            // btnOpenDirectorySearchPanel
            // 
            this.btnOpenDirectorySearchPanel.AutoSize = true;
            this.btnOpenDirectorySearchPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenDirectorySearchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOpenDirectorySearchPanel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOpenDirectorySearchPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.btnOpenDirectorySearchPanel.Location = new System.Drawing.Point(0, 56);
            this.btnOpenDirectorySearchPanel.Name = "btnOpenDirectorySearchPanel";
            this.btnOpenDirectorySearchPanel.Size = new System.Drawing.Size(457, 260);
            this.btnOpenDirectorySearchPanel.TabIndex = 4;
            this.btnOpenDirectorySearchPanel.Text = "Start to analysis";
            this.btnOpenDirectorySearchPanel.UseVisualStyleBackColor = true;
            this.btnOpenDirectorySearchPanel.Visible = false;
            // 
            // labelFilePathInfo
            // 
            this.labelFilePathInfo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelFilePathInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.labelFilePathInfo.Location = new System.Drawing.Point(0, 316);
            this.labelFilePathInfo.Margin = new System.Windows.Forms.Padding(3);
            this.labelFilePathInfo.Name = "labelFilePathInfo";
            this.labelFilePathInfo.Size = new System.Drawing.Size(457, 20);
            this.labelFilePathInfo.TabIndex = 3;
            this.labelFilePathInfo.Text = "Please select directory and put filename for save analyse result";
            this.labelFilePathInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSelectFileForSave
            // 
            this.btnSelectFileForSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectFileForSave.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSelectFileForSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelectFileForSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.btnSelectFileForSave.Location = new System.Drawing.Point(0, 336);
            this.btnSelectFileForSave.Name = "btnSelectFileForSave";
            this.btnSelectFileForSave.Size = new System.Drawing.Size(457, 36);
            this.btnSelectFileForSave.TabIndex = 2;
            this.btnSelectFileForSave.Text = "Save as";
            this.btnSelectFileForSave.UseVisualStyleBackColor = true;
            // 
            // labelDirPathInfo
            // 
            this.labelDirPathInfo.AutoSize = true;
            this.labelDirPathInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelDirPathInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.labelDirPathInfo.Location = new System.Drawing.Point(0, 36);
            this.labelDirPathInfo.Margin = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.labelDirPathInfo.Name = "labelDirPathInfo";
            this.labelDirPathInfo.Size = new System.Drawing.Size(296, 20);
            this.labelDirPathInfo.TabIndex = 1;
            this.labelDirPathInfo.Text = "Please, select directory folder for analyse";
            this.labelDirPathInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSelectDir
            // 
            this.btnSelectDir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectDir.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSelectDir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelectDir.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.btnSelectDir.Location = new System.Drawing.Point(0, 0);
            this.btnSelectDir.Name = "btnSelectDir";
            this.btnSelectDir.Size = new System.Drawing.Size(457, 36);
            this.btnSelectDir.TabIndex = 0;
            this.btnSelectDir.Text = "Browse for directory";
            this.btnSelectDir.UseVisualStyleBackColor = true;
            // 
            // TreeViewPanel
            // 
            this.TreeViewPanel.Controls.Add(this.labelTreeViewInformation);
            this.TreeViewPanel.Controls.Add(this.treeViewResult);
            this.TreeViewPanel.Controls.Add(this.btnBackToIntroPanel);
            this.TreeViewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeViewPanel.Location = new System.Drawing.Point(0, 0);
            this.TreeViewPanel.Name = "TreeViewPanel";
            this.TreeViewPanel.Size = new System.Drawing.Size(457, 372);
            this.TreeViewPanel.TabIndex = 1;
            // 
            // labelTreeViewInformation
            // 
            this.labelTreeViewInformation.AutoSize = true;
            this.labelTreeViewInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.labelTreeViewInformation.Location = new System.Drawing.Point(88, 12);
            this.labelTreeViewInformation.Name = "labelTreeViewInformation";
            this.labelTreeViewInformation.Size = new System.Drawing.Size(78, 17);
            this.labelTreeViewInformation.TabIndex = 2;
            this.labelTreeViewInformation.Text = "Information";
            // 
            // treeViewResult
            // 
            this.treeViewResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeViewResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewResult.Location = new System.Drawing.Point(0, 36);
            this.treeViewResult.Name = "treeViewResult";
            this.treeViewResult.Size = new System.Drawing.Size(457, 336);
            this.treeViewResult.TabIndex = 1;
            // 
            // btnBackToIntroPanel
            // 
            this.btnBackToIntroPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.btnBackToIntroPanel.Location = new System.Drawing.Point(0, 0);
            this.btnBackToIntroPanel.Name = "btnBackToIntroPanel";
            this.btnBackToIntroPanel.Size = new System.Drawing.Size(82, 36);
            this.btnBackToIntroPanel.TabIndex = 0;
            this.btnBackToIntroPanel.Text = "Back";
            this.btnBackToIntroPanel.UseVisualStyleBackColor = true;
            // 
            // DirectoryAnalyzerView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(457, 372);
            this.Controls.Add(this.TreeViewPanel);
            this.Controls.Add(this.IntroPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "DirectoryAnalyzerView";
            this.Text = "Directory Analyzer";
            this.IntroPanel.ResumeLayout(false);
            this.IntroPanel.PerformLayout();
            this.TreeViewPanel.ResumeLayout(false);
            this.TreeViewPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel IntroPanel;
        private System.Windows.Forms.Button btnSelectDir;
        private System.Windows.Forms.Label labelDirPathInfo;
        private System.Windows.Forms.Label labelFilePathInfo;
        private System.Windows.Forms.Button btnSelectFileForSave;
        private System.Windows.Forms.Button btnOpenDirectorySearchPanel;
        private System.Windows.Forms.Panel TreeViewPanel;
        private System.Windows.Forms.Button btnBackToIntroPanel;
        private System.Windows.Forms.TreeView treeViewResult;
        private System.Windows.Forms.Label labelTreeViewInformation;
    }
}