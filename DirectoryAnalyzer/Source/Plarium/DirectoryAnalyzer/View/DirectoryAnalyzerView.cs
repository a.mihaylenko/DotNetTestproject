﻿using Plarium.Common.Controller;
using Plarium.Common.View;
using Plarium.DirectoryAnalyzer.Model.Vo;
using System;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using static Plarium.DirectoryAnalyzer.Enums;

namespace Plarium.DirectoryAnalyzer.View
{
    public partial class DirectoryAnalyzerView : Form, IView
    {
        #region Private properties
        private IController _controller;
        #endregion
        #region Constructor
        public DirectoryAnalyzerView()
        {
            InitializeComponent();
            //Set all Panels visible false
            foreach (Panel panel in Controls.OfType<Panel>())
                panel.Visible = false;

        }


        #endregion

        #region Public methods
        public void SetController(IController controller)
        {
            _controller = controller;
        }

        public void ChangeViewPanel(AppViewPanel appViewPanel)
        {
            string panelName = appViewPanel.ToString();
            Type thisType = GetType();
            MethodInfo methodInfo;
            string InitPrefix = "Init";
            string ClosePrefix = "Close";
            bool isVisible;
            foreach (Panel panel in Controls.OfType<Panel>())
            {
                isVisible = panel.Name == panelName;

                methodInfo = thisType.GetMethod(string.Format("{0}_{1}", panel.Name, isVisible? InitPrefix : ClosePrefix), BindingFlags.NonPublic | BindingFlags.Instance);
                if (methodInfo != null)
                {
                    methodInfo.Invoke(this, null);
                }
                panel.Visible = isVisible;
            }
        }

        #endregion
        #region Overided inherit methods
        /// <inheritdoc />
        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            _controller.ExecuteCommand( (int)Command.ShowViewIntroPanel );
        }
        #endregion
        #region View panel methods( executing before set panel as Visible )
        #region TreeViewPanel
        private void TreeViewPanel_Init()
        {
            btnBackToIntroPanel.Click += BtnBackToIntroPanel_Click;
        }

        private void TreeViewPanel_Close()
        {
            treeViewResult.Nodes.Clear();
            btnBackToIntroPanel.Click -= BtnBackToIntroPanel_Click;

        }
        private void BtnBackToIntroPanel_Click(object sender, EventArgs e)
        {
            _controller.ExecuteCommand((int)Command.ShowViewIntroPanel);
        }
        public void DrawViewTreeNode(FileInfoNodeVO fileInfoNodeVO)
        {
            //TreeNodeCollection parentNodes = fileInfoNodeVO.ParenId != null ? treeViewResult.Nodes[fileInfoNodeVO.ParenId].Nodes : treeViewResult.Nodes;
            TreeNode[] treeNodes = treeViewResult.Nodes.Find(fileInfoNodeVO.ParenId, true);
            TreeNodeCollection parentNodes = treeNodes.Length>0 ? treeNodes[0].Nodes : treeViewResult.Nodes;
            TreeNode treeNode = parentNodes.Add(fileInfoNodeVO.Id, fileInfoNodeVO.Name);
            treeNode.BackColor = (fileInfoNodeVO.FileAttributes & System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory ? Color.Yellow : Color.Gray;

            if (fileInfoNodeVO.AccessRights.Read)
            {
                treeNode.Nodes.Add(fileInfoNodeVO.Id, "CreationDate").Nodes.Add(fileInfoNodeVO.CreationDate.ToLongDateString());
                treeNode.Nodes.Add(fileInfoNodeVO.Id, "ModificationDate").Nodes.Add(fileInfoNodeVO.ModificationDate.ToLongDateString());
                treeNode.Nodes.Add(fileInfoNodeVO.Id, "LastAccessDate").Nodes.Add(fileInfoNodeVO.LastAccessDate.ToLongDateString());

                TreeNode treeNodeAttributes = treeNode.Nodes.Add("Attributes");
                treeNodeAttributes.Nodes.Add("ReadOnly").Nodes.Add(fileInfoNodeVO.Attributes.ReadOnly.ToString());
                treeNodeAttributes.Nodes.Add("Hidden").Nodes.Add(fileInfoNodeVO.Attributes.Hidden.ToString());
                treeNodeAttributes.Nodes.Add("System").Nodes.Add(fileInfoNodeVO.Attributes.System.ToString());



                treeNode.Nodes.Add(fileInfoNodeVO.Id, "Size").Nodes.Add(fileInfoNodeVO.Length.ToString());
                treeNode.Nodes.Add(fileInfoNodeVO.Id, "Owner").Nodes.Add(fileInfoNodeVO.Owner);
            }
            TreeNode assessRights = treeNode.Nodes.Add("Access rights for current user");
            assessRights.Nodes.Add("Read").Nodes.Add(fileInfoNodeVO.AccessRights.Read.ToString());
            assessRights.Nodes.Add("Write").Nodes.Add(fileInfoNodeVO.AccessRights.Write.ToString());
            assessRights.Nodes.Add("Delete").Nodes.Add(fileInfoNodeVO.AccessRights.Delete.ToString());


            //TreeNode node = new TreeNode(fileInfoNodeVO.Name);
        }
        public void TreeViewPanel_UpdateInfo( string parsingFilePath )
        {
            labelTreeViewInformation.Text = parsingFilePath;
        }
        public void SetEnableBackToIntro( bool value)
        {
            btnBackToIntroPanel.Enabled = value;
        }
        #endregion
        #region IntroPanel
        private string _defaultDirPath=null;
        private string _defaultfilePathInfo = null;
        /// <summary>
        /// Initialize itro panel components
        /// </summary>
        private void IntroPanel_Init()
        {
            btnOpenDirectorySearchPanel.Visible = false;
            btnSelectDir.Click += BtnSelectDir_Click;
            btnSelectFileForSave.Click += BtnSelectFileForSave_Click;
            btnOpenDirectorySearchPanel.Click += BtnOpenDirectorySearchPanel_Click;
        }



        /// <summary>
        /// Set empty intro panel, remove UI event handlers 
        /// and command dependencies
        /// </summary>
        private void IntroPanel_Close()
        {
            btnSelectDir.Click -= BtnSelectDir_Click;
            btnSelectFileForSave.Click -= BtnSelectFileForSave_Click;
            btnOpenDirectorySearchPanel.Click -= BtnOpenDirectorySearchPanel_Click;
        }
        #region Intro panel buttons event handlers
        private void BtnSelectFileForSave_Click(object sender, EventArgs e)
        {
            _controller.ExecuteCommand((int)Command.SelectFileForSaveResult);
        }
        private void BtnSelectDir_Click(object sender, EventArgs e)
        {
            _controller.ExecuteCommand((int)Command.SelectDirectoryForAnalyze);
        }
        #endregion
        #region Intro panel methods for commands
        public void IntoPanel_UpdateDirPathInfo( string path )
        {
            _defaultDirPath = _defaultDirPath??labelDirPathInfo.Text;
            labelDirPathInfo.Text = path??_defaultDirPath;
        }
        public void IntoPanel_UpdateFilePathInfo(string path)
        {
            _defaultfilePathInfo = _defaultfilePathInfo ?? labelFilePathInfo.Text;
            labelFilePathInfo.Text = path??_defaultfilePathInfo;
        }
        public void IntoPanel_SetVisibleBtnOpenNextPanel( bool value )
        {
            btnOpenDirectorySearchPanel.Visible = value;
        }
        private void BtnOpenDirectorySearchPanel_Click(object sender, EventArgs e)
        {
            _controller.ExecuteCommand((int)Command.ShowViewResultTreePanel);
        }
        #endregion
        #endregion
        #endregion
    }
}
